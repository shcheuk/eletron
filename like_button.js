'use strict';
const { ipcRenderer } = require('electron');
const e = React.createElement;

class LikeButton extends React.Component {
	constructor(props) {
		super(props);
		this.state = { liked: false };
	}

	render() {
		if (this.state.liked) {
			ipcRenderer.send('close', { by: 'like_button' });
			return 'I am closing in 3 seconds';
		}
		return e('button', { onClick: () => this.setState({ liked: true }) }, 'Close');
	}
}
const domContainer = document.querySelector('#like_button_container');
ReactDOM.render(e(LikeButton), domContainer);
