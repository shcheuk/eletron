const path = require('path');
const { app, BrowserWindow, ipcMain } = require('electron');
let win;
function createWindow() {
	win = new BrowserWindow({
		width: 1024,
		height: 600,
		webPreferences: {
			nodeIntegration: true,
			contextIsolation: false,
			preload: path.join(__dirname, 'preload.js')
		}
	});

	win.loadFile('index.html');
	win.webContents.openDevTools();
}
app.whenReady().then(() => {
	createWindow();
	app.on('activate', function () {
		if (BrowserWindow.getAllWindows().length === 0) createWindow();
	});
});
app.on('window-all-closed', function () {
	if (process.platform !== 'darwin') app.quit();
});

ipcMain.on('close', () => {
	win.webContents.send('reply to event1', '');
	console.log('close clicked, closing in 5 seconds');
	setTimeout(() => {
		app.exit();
	}, 3000);
});
